package com.lzb.li.testzifu;

import android.text.TextUtils;

/**
 * @author luzhenbang
 * @Description TODO 支付结果
 * @date 2015/12/5
 * @Copyright: Copyright (c) 2015 Shenzhen Tentinet Technology Co., Ltd. Inc. All rights reserved.
 */
public class PayResult {
    /**状态码*/
    private String resultStatus;
    /**支付结果*/
    private String result;

    private String memo;

    /**
     *
     *
     * @version 1.0
     *
     * @createTime 2015/12/5,16:49
     * @updateTime 2015/12/5,16:49
     * @createAuthor luzhenbang
     * @updateAuthor
     * @updateInfo (此处输入修改内容,若无修改可不写.)
     * @param rawResult 支付响应结果
     */
    public PayResult(String rawResult) {

        if (TextUtils.isEmpty(rawResult))
            return;

        String[] resultParams = rawResult.split(";");
        for (String resultParam : resultParams) {
            if (resultParam.startsWith("resultStatus")) {
                resultStatus = gatValue(resultParam, "resultStatus");
            }
            if (resultParam.startsWith("result")) {
                result = gatValue(resultParam, "result");
            }
            if (resultParam.startsWith("memo")) {
                memo = gatValue(resultParam, "memo");
            }
        }
    }

    @Override
    public String toString() {
        return "resultStatus={" + resultStatus + "};memo={" + memo
                + "};result={" + result + "}";
    }

    private String gatValue(String content, String key) {
        String prefix = key + "={";
        return content.substring(content.indexOf(prefix) + prefix.length(),
                content.lastIndexOf("}"));
    }

    /**
     * @return the resultStatus
     */
    public String getResultStatus() {
        return resultStatus;
    }

    /**
     * @return the memo
     */
    public String getMemo() {
        return memo;
    }

    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }
}
